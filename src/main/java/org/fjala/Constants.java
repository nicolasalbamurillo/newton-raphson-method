package org.fjala;

import java.math.MathContext;
import java.math.RoundingMode;

public class Constants {

    public static MathContext MATH_CONTEXT = new MathContext(25, RoundingMode.HALF_UP);
}
