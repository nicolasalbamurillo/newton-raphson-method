package org.fjala;

import java.math.BigDecimal;

/**
 * A functional interface that represents a precise function in Calculation with derivative
 * function, integration function and newton method.
 *
 * @author Nicolas Alba Murillo
 */
@FunctionalInterface
public interface IBigDecimalFunction {

    BigDecimal H = new BigDecimal("0.000000000001");
    BigDecimal DELTA = new BigDecimal("0.000001");

    BigDecimal N = new BigDecimal("10000000");

    BigDecimal f(BigDecimal x);

    default BigDecimal f(double x) {
        return f(BigDecimal.valueOf(x));
    }

    /** Get the first derivative. */
    default BigDecimal derive(BigDecimal x) {
        var y1 = f(x.add(H));
        BigDecimal deltaX = y1.subtract(f(x));
        return deltaX.divide(H);
    }

    /** Get the n-esima derivative. */
    default BigDecimal derive(BigDecimal x, int n) {
        if (n == 1) {
            return derive(x);
        } else if (n < 1) {
            throw new IllegalArgumentException("Cannot be negative");
        }
        var deltaY = derive(x.add(H), n - 1).subtract(derive(x, n - 1));
        return deltaY.divide(H);
    }

    default BigDecimal integrate(BigDecimal x) {
        var base = x.divide(N, Constants.MATH_CONTEXT);
        var heightSummation = BigDecimal.ZERO;
        for (BigDecimal i = BigDecimal.ZERO;
                i.compareTo(N) != 0;
                i = i.add(BigDecimal.ONE, Constants.MATH_CONTEXT)) {
            var xi = base.multiply(i, Constants.MATH_CONTEXT);
            var height = this.f(xi);
            heightSummation = heightSummation.add(height, Constants.MATH_CONTEXT);
        }
        return heightSummation.multiply(base, Constants.MATH_CONTEXT);
    }

    /** View the newton raphson method, H is the delta or error. */
    default BigDecimal newton(BigDecimal equalTo, BigDecimal approximate) {
        IBigDecimalFunction sqrt = x -> this.f(x).subtract(equalTo);
        var x = approximate;
        var currentResult = this.f(x);
        var error = currentResult.subtract(equalTo);
        int count = 1;
        while (error.abs().compareTo(DELTA) >= 0) {
            BigDecimal f = sqrt.f(x);
            var derive = sqrt.derive(x);
            var h = f.divide(derive, Constants.MATH_CONTEXT);
            x = x.subtract(h);
            // view error
            currentResult = this.f(x);
            error = currentResult.subtract(equalTo);
            System.out.print(count++ + ", Error is: ");
            System.out.println(error);
        }
        return x;
    }

    /** View newton raphson Modify method. */
    default BigDecimal newtonModify(BigDecimal equalTo, BigDecimal approximate) {
        IBigDecimalFunction sqrt = x -> this.f(x).subtract(equalTo);
        var x = approximate;
        var currentResult = this.f(x);
        var error = currentResult.subtract(equalTo);
        int count = 1;
        while (error.abs().compareTo(DELTA) >= 0) {
            BigDecimal f = sqrt.f(x);
            var derive = sqrt.derive(x);
            var derive2 = sqrt.derive(x, 2);
            var numerator = f.multiply(derive);
            var denominator = derive.multiply(derive).subtract(f.multiply(derive2));
            var h = numerator.divide(denominator, Constants.MATH_CONTEXT);
            x = x.subtract(h);
            // view error
            currentResult = this.f(x);
            error = currentResult.subtract(equalTo);
            System.out.print(count++ + ", Error is: ");
            System.out.println(error);
        }
        return x;
    }
}
